var a = 5;
var b = 10;

if (a === 5) {
  let a = 4; // El alcance es dentro del bloque if
  var b = 1; // El alcance es dentro de la función

  console.log(a); // 4
  console.log(b); // 1
}

console.log(a); // 5
console.log(b); // 1

let variable2 = 2;

function cualquiera() {
  if (2 == 2) {
    // por poner algo
    var variable1 = 1;
    variable2 = 2;
  }
  // aqui variable1 existe
  // aqui variable2 no existe
  console.log(variable1);
}
